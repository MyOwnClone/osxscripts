#!/bin/bash
# Init
if [ -n "$1" ]; then
	FILE="/tmp/out.$$"
	GREP="/bin/grep"
	#....
	# Make sure only root can run our script
	if [ "$(id -u)" != "0" ]; then
	   echo "This script must be run as root" 1>&2
	   exit 1
	fi

	# Save everything from /Volumes/ramdisk to a backup directory /var/spool/rdimage
	sudo rsync -av --delete /Volumes/ramdisk/ /var/spool/rdimage/
	sudo diskutil unmount /dev/disk$1
	sudo diskutil eject /dev/disk$1
else
	echo "Parametter is missing!!!" 1>&2
fi

