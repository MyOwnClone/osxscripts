#!/bin/bash
# Init
FILE="/tmp/out.$$"
GREP="/bin/grep"
#....
# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

sudo diskutil erasevolume HFS+ "ramdisk" `hdiutil attach -nomount ram://4194304`
sudo diskutil enableOwnership /Volumes/ramdisk

#restore ramdisk image from disk
sudo rsync -a /var/spool/rdimage/ /Volumes/ramdisk/
