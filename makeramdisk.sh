# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

diskutil erasevolume HFS+ "ramdisk" `hdiutil attach -nomount ram://4194304`