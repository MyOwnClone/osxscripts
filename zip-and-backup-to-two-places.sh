name=$1-$(date +"%m-%d-%Y-%H-%M-%S").zip
zip -r $name $1
cp $name $2
mv $name $3
