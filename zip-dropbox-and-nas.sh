name=$1-$(date +"%m-%d-%Y-%H-%M-%S").zip
zip -r $name $1
cp $name ~/Dropbox/regular-backup
cp $name /Volumes/tomas/regular-backups
rm $name
